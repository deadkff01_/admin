const webpack = require('webpack')

const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default

// constantes com os caminhos dos arquivos
// const paths = {
//     DIST: path.resolve(__dirname, 'dist'),
//     SRC: path.resolve(__dirname, 'src'),
//     JS: path.resolve(__dirname, 'src/js'),
// };
// faz um hook guarda arquivos js no cache
require("@babel/register");

const config = {
    entry: ['@babel/polyfill', './resources/js/app.js'],
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'public/js'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    resolve: {},
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: '../css/mycss.css',
            //chunkFilename: '[id].[hash].css',
        }),
        new OptimizeCSSAssetsPlugin(),
        new CopyWebpackPlugin(
            [
                {
                    from: path.resolve(__dirname, 'resources/font/'),
                    to: path.resolve(__dirname, 'public/font/')
                },
                {
                    from: path.resolve(__dirname, 'resources/img/'),
                    to: path.resolve(__dirname, 'public/img/')
                },
            ]),
        new ImageminPlugin({
            test: /\.(jpe?g|png|gif|svg)$/i,
            pngquant: {
                quality: '95-100'
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: { sourceMap: true }
                    },
                    {
                        loader: 'sass-loader',
                        options: { sourceMap: true }
                    }
                    //'postcss-loader',
                ],
            },
            {
                test: /\.(woff2?|ttf|svg|eot|png|jpg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader',
            }
        ]
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({
                assetNameRegExp: /\.css$/,
                cssProcessor: require('cssnano'),
                cssProcessorPluginOptions: {
                    preset: ['default', { discardComments: { removeAll: true } }],
                },
                canPrint: true
            })
        ]
    },
    // OPCIONAL
    // reload 
    watch: true,
    // Development Tools (Map Errors To Source File)
    devtool: 'source-map',
};
// Exports
module.exports = config;
